package jdbcrest.controller.mysql.utils;

public enum Entity {
    CARGO("cargo"),CITY("city"),DELIVERY("delivery"),INTERACTION_WITH_CLIENT("interaction_with_client"),
    PERSON("person"),PERSONAL_DATA("personal_data"),POST_OFFICE("post_office");

    private String value;

    public String getValue() {
        return value;
    }

    Entity(String value) {
        this.value = value;
    }
    }
