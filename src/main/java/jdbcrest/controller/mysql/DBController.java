package jdbcrest.controller.mysql;

import jdbcrest.controller.mysql.core.DBClient;
import jdbcrest.controller.mysql.core.DBQueryExecutor;
import jdbcrest.controller.mysql.utils.Entity;
import jdbcrest.controller.mysql.utils.Operation;
import jdbcrest.controller.mysql.utils.QueryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DBController {
    private static Logger LOG = LogManager.getLogger(DBController.class.getName());

    public static ResultSet select(Entity tableName, String statement) throws SQLException {
        return DBQueryExecutor.executeSelect(QueryBuilder.buildQuery(Operation.SELECT, tableName, statement));
    }

    public static void insert(Entity tableName, String fields, String values) throws SQLException {
        LOG.debug("Database controller trying to do INSERT.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.INSERT, tableName, fields, values));
        DBClient.setForeignKeyCheck(true);
    }

    public static void delete(Entity tableName, String statement) throws SQLException {
        LOG.debug("Database controller trying to do DELETE.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.DELETE, tableName, statement));
        DBClient.setForeignKeyCheck(true);
    }

    public static void update(Entity tableName, String field, String newValue, String statement) throws SQLException {
        LOG.debug("Database controller trying to do UPDATE.");
        DBClient.setForeignKeyCheck(false);
        DBQueryExecutor.execute(QueryBuilder.buildQuery(Operation.UPDATE, tableName, field, newValue, statement));
        DBClient.setForeignKeyCheck(true);
    }


}
