package jdbcrest.controller.rest.core;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdbcrest.controller.mysql.DBController;
import jdbcrest.controller.mysql.utils.Entity;
import jdbcrest.controller.rest.utils.ResultSetParser;
import jdbcrest.entity.personal_data.PersonalData;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("api/personal")
@Api(value = "PersonalData", tags = {"PersonalData"})
public class PersonalDataController {
    @ApiOperation(value = "Get PersonalData by id", response = Iterable.class, tags = "PersonalData")
    @GetMapping(value = "/get/{id}")
    public PersonalData getPersonalData(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        return (PersonalData) ResultSetParser.parseResultSet(DBController.select(Entity.PERSONAL_DATA, "id=" + id), PersonalData.class);
    }

    @ApiOperation(value = "Delete PersonalData by id", response = Iterable.class, tags = "PersonalData")
    @DeleteMapping(value = "/delete/{id}")
    public void deletePersonalData(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        DBController.delete(Entity.PERSONAL_DATA, "id=" + id);
    }

    @ApiOperation(value = "Put PersonalData(only change mobile_number)", response = PersonalData.class, tags = "PersonalData")
    @PutMapping(value = "/put/{id},{mobile_number}")
    public PersonalData putPersonalData(@PathVariable("id") String id, @ApiParam(required = true, value = "new PersonalData(Example - +380982820760)", name = "PersonalData")
    @RequestBody() String mobile_number) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.update(Entity.PERSONAL_DATA, "mobile_number", mobile_number, "id=" + id);
        return (PersonalData) ResultSetParser.parseResultSet(DBController.select(Entity.PERSONAL_DATA, "id=" + id), PersonalData.class);
    }

    @ApiOperation(value = "Post new PersonalData", response = PersonalData.class, tags = "PersonalData")
    @PostMapping(value = "/post")
    public void postPersonalData(@ApiParam(required = true, value = "new PersonalData(Example - 'Ivan', 'Gerasymovich', '+380988752760', 'ivan590@gmail.com')", name = "PersonalData json")
                                 @RequestBody String body) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.insert(Entity.PERSONAL_DATA, "name, surname, mobile_number, email", body);
    }
}
