package jdbcrest.controller.rest.core;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdbcrest.controller.mysql.DBController;
import jdbcrest.controller.mysql.utils.Entity;
import jdbcrest.controller.rest.utils.ResultSetParser;
import jdbcrest.entity.cargo.Cargo;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("api/cargo")
@Api(value = "Cargo", tags = {"Cargo"})
public class CargoController {
    @ApiOperation(value = "Get cargo by id", response = Cargo.class, tags = "Cargo")
    @GetMapping(value = "/get/{id}")
    public Cargo getCargo(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        return (Cargo) ResultSetParser.parseResultSet(DBController.select(Entity.CARGO, "id=" + id), Cargo.class);
    }

    @ApiOperation(value = "Delete cargo by id", response = Iterable.class, tags = "Cargo")
    @DeleteMapping(value = "/delete/{id}")
    public void deleteCargo(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        DBController.delete(Entity.CARGO, "id=" + id);
    }

    @ApiOperation(value = "Put cargo(only change name)", response = Cargo.class, tags = "Cargo")
    @PutMapping(value = "/put/{id},{name}")
    public Cargo putCargo(@PathVariable("id") String id, @ApiParam(required = true, value = "new Cargo(Example - NewName)", name = "Cargo")
    @RequestBody() String name) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.update(Entity.CARGO, "name", name, "id=" + id);
        return (Cargo) ResultSetParser.parseResultSet(DBController.select(Entity.CARGO, "id=" + id), Cargo.class);
    }

    @ApiOperation(value = "Post new cargo", response = Cargo.class, tags = "Cargo")
    @PostMapping(value = "/post")
    public void postCargo(@ApiParam(required = true, value = "new Cargo(Example - \"Redmi note 5\", 500, \"phone\", 64)", name = "Cargo json")
                          @RequestBody String body) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.insert(Entity.CARGO, "name, weight, type, client_id", body);
    }
}
