package jdbcrest.controller.rest.core;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdbcrest.controller.mysql.DBController;
import jdbcrest.controller.mysql.utils.Entity;
import jdbcrest.controller.rest.utils.ResultSetParser;
import jdbcrest.entity.delivery.Delivery;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("api/delivery")
@Api(value = "Delivery", tags = {"Delivery"})
public class DeliveryController {
    @ApiOperation(value = "Get delivery by id", response = Iterable.class, tags = "Delivery")
    @GetMapping(value = "/get/{id}")
    public Delivery getDelivery(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        return (Delivery) ResultSetParser.parseResultSet(DBController.select(Entity.DELIVERY, "id=" + id), Delivery.class);
    }

    @ApiOperation(value = "Delete delivery by id", response = Iterable.class, tags = "Delivery")
    @DeleteMapping(value = "/delete/{id}")
    public void deleteDelivery(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        DBController.delete(Entity.DELIVERY, "id=" + id);
    }

    @ApiOperation(value = "Put delivery(only change delivery date)", response = Delivery.class, tags = "Delivery")
    @PutMapping(value = "/put/{id},{delivery_date}")
    public Delivery putDelivery(@PathVariable("id") String id, @ApiParam(required = true, value = "new Delivery(Example - NewName)", name = "Delivery")
    @RequestBody() String delivery_date) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.update(Entity.CARGO, "delivery_date", delivery_date, "id=" + id);
        return (Delivery) ResultSetParser.parseResultSet(DBController.select(Entity.DELIVERY, "id=" + id), Delivery.class);
    }

    @ApiOperation(value = "Post new delivery", response = Delivery.class, tags = "Delivery")
    @PostMapping(value = "/post")
    public void postDelivery(@ApiParam(required = true, value = "new Delivery(Example - 1, 2, \"2019-11-30\", 2,3)", name = "Delivery json")
                             @RequestBody String body) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.insert(Entity.CARGO, "courier_id, delivery_date, post_office_id, cargo_id", body);
    }
}
