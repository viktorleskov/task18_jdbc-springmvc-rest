package jdbcrest.controller.rest.core;

import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdbcrest.controller.mysql.DBController;
import jdbcrest.controller.mysql.utils.Entity;
import jdbcrest.controller.rest.utils.ResultSetParser;
import jdbcrest.entity.city.City;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("api/city")
@Api(value = "City", tags = {"City"})
public class CityController {
    @ApiOperation(value = "Get city by id", response = City.class, tags = "City")
    @GetMapping(value = "/get/{id}")
    public City getCity(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        return (City) ResultSetParser.parseResultSet(DBController.select(Entity.CITY, "id=" + id), City.class);
    }

    @ApiOperation(value = "Delete city by id", response = Iterable.class, tags = "City")
    @DeleteMapping(value = "/delete/{id}")
    public void deleteCity(@PathVariable String id) throws SQLException, InstantiationException, IllegalAccessException {
        DBController.delete(Entity.CITY, "id=" + id);
    }

    @ApiOperation(value = "Put City(only change name)", response = City.class, tags = "City")
    @PutMapping(value = "/put/{id},{name}")
    public City putCity(@PathVariable("id") String id, @ApiParam(required = true, value = "new City(Example - newCityName)", name = "City")
    @RequestBody() String name) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.update(Entity.CITY, "name", name, "id=" + id);
        return (City) ResultSetParser.parseResultSet(DBController.select(Entity.CITY, "id=" + id), City.class);
    }

    @ApiOperation(value = "Post new city", response = City.class, tags = "City")
    @PostMapping(value = "/post")
    public void postCity(@ApiParam(required = true, value = "new City(Example - \"Lviv\")", name = "City json")
                         @RequestBody String body) throws SQLException, InstantiationException, IllegalAccessException, MessagingException {
        DBController.insert(Entity.CITY, "name", body);
    }
}
