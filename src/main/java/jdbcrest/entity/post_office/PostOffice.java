package jdbcrest.entity.post_office;

public class PostOffice {
    private int id;
    private String street;
    private int cityId;

    public PostOffice(int id, String street, int cityId) {
        this.id = id;
        this.street = street;
        this.cityId = cityId;
    }

    public int getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public int getCityId() {
        return cityId;
    }

    @Override
    public String toString() {
        return "PostOffice{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", cityId=" + cityId +
                '}';
    }
}
