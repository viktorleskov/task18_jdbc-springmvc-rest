package jdbcrest.entity.personal_data;

public class PersonalData {
    private int id;
    private String name;
    private String surname;
    private String mobile_number;
    private String email;

    public PersonalData(int id, String name, String surname, String mobile_number, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mobile_number = mobile_number;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "PersonalData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mobile_number='" + mobile_number + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

}
