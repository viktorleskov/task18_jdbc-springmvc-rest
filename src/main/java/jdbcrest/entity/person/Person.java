package jdbcrest.entity.person;

public abstract class Person {
    private int id;
    private int personalDataId;

    Person(int id, int personalDataId) {
        this.id = id;
        this.personalDataId = personalDataId;
    }

    public int getId() {
        return id;
    }

    public int getPersonalDataId() {
        return personalDataId;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", personalDataId=" + personalDataId +
                '}';
    }
}
