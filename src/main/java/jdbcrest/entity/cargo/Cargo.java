package jdbcrest.entity.cargo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;

@Getter
@NoArgsConstructor
@ApiModel(value = "Cargo")
public class Cargo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "weight")
    private int weight;
    @Column(name = "type")
    private String type;
    @Column(name = "client_id")
    private int client_id;

    public Cargo(int id, String name, int weight, String type, int clientId) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.type = type;
        this.client_id = clientId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getType() {
        return type;
    }

    public int getClient_id() {
        return client_id;
    }

    @Override
    public String toString() {
        return "Cargo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", type='" + type + '\'' +
                ", clientId=" + client_id +
                '}';
    }
}
