package jdbcrest.entity.delivery;

public class Delivery {
    private int id;
    private int courier_id;
    private String delivery_date;
    private int post_office_id;
    private int cargo_id;

    public Delivery(int id, int courier_id, String delivery_date, int post_office_id, int cargo_id) {
        this.id = id;
        this.courier_id = courier_id;
        this.delivery_date = delivery_date;
        this.post_office_id = post_office_id;
        this.cargo_id = cargo_id;
    }

    public int getId() {
        return id;
    }

    public int getCourier_id() {
        return courier_id;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public int getPost_office_id() {
        return post_office_id;
    }

    public int getCargo_id() {
        return cargo_id;
    }

    @Override
    public String toString() {
        return "Delivery{" +
                "id=" + id +
                ", courier_id=" + courier_id +
                ", delivery_date='" + delivery_date + '\'' +
                ", post_office_id=" + post_office_id +
                ", cargo_id=" + cargo_id +
                '}';
    }
}
