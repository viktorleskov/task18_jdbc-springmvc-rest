package jdbcrest.entity.interactionwithclient;

public class InteractionWithClient {
    private int id;
    private int operator_id;
    private int client_id;

    public InteractionWithClient(int id, int operator_id, int client_id) {
        this.id = id;
        this.operator_id = operator_id;
        this.client_id = client_id;
    }

    public int getId() {
        return id;
    }

    public int getOperator_id() {
        return operator_id;
    }

    public int getClient_id() {
        return client_id;
    }

    @Override
    public String toString() {
        return "InteractionWithClient{" +
                "id=" + id +
                ", operator_id=" + operator_id +
                ", client_id=" + client_id +
                '}';
    }
}
